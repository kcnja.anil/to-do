import React from 'react';
import './App.css';

export default class ToDo extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      newTask:"",
      toDoList:[]
    }
    this.addTask = this.addTask.bind(this)
    // this.deleteTask = this.deleteTask.bind(this)
   this.count = 0;
  }
  
  addTask(){
    const newTask = {
      taskID : this.count++,
      task : this.state.newTask
    };
    const list = this.state.toDoList;
    list.push(newTask);
    this.setState({
      list, newTask:""
    })
  }

  deleteTask(id){
    const list = this.state.toDoList;
    const updatedList = list.filter(task => task.taskID !== id)
    this.setState({
      toDoList : updatedList
    })
  }

  updateInput(value){
    this.setState({
      newTask : value
    })
  }
  render(){
    return(
      <div class="todo">
        <h1>To Do</h1> <br/>
        <input class="todo_input" placeholder = "Add your task here" value = {this.state.newTask} onChange = {e => this.updateInput( e.target.value)}/>
        {this.state.newTask !== "" ?
          <button class="add_task" onClick={this.addTask}>Add task</button>
          :null
        }
        <br/>
        <div>
          <ol>
            {this.state.toDoList.map(task => {
              return(
                <li key = {task.taskID} class="task_list">
                  {task.task}
                  <button class="delete_task" onClick = {() => this.deleteTask(task.taskID)}>X</button>
                </li>
              )
            })}
          </ol>
        </div>
      </div>
    )
  }
}
